﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Game : MonoBehaviour
{
    public Slider slider;
    public Text text;
    public float offset = 1f;
    public float period = 0.5f;
    float nextActionTime = 0f;
    float previousActionTime = 0f;
    bool goingNegative;
    // Start is called before the first frame update
    void Start()
    {
        slider.value = slider.minValue;
        nextActionTime = 0f;
        goingNegative = false;
    }

    // Update is called once per frame
    void Update()
    {
        previousActionTime += Time.deltaTime;

        if (previousActionTime > nextActionTime)
        {
            nextActionTime += period;
            // execute block of code here
            slider.value += goingNegative ? (-1 * offset) : offset;
            text.text = slider.value.ToString();
            if (slider.value >= slider.maxValue)
            {
                goingNegative = true;
            }
            else if (slider.value <= slider.minValue)
            {
                goingNegative = false;
            }
        }
    }
}
